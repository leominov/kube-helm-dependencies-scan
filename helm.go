package main

import (
	"bytes"
	"compress/gzip"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"sort"
	"strings"

	"helm.sh/helm/v3/pkg/chart"
	rspb "helm.sh/helm/v3/pkg/release"
)

var (
	magicGzip = []byte{0x1f, 0x8b, 0x08}
)

func ChartLockToString(lock *chart.Lock, filter []string) string {
	if lock == nil {
		return ""
	}
	var deps []string
	for _, dep := range lock.Dependencies {
		var match bool
		for _, f := range filter {
			if f == dep.Name {
				match = true
				break
			}
		}
		if !match {
			continue
		}
		line := fmt.Sprintf("%s@%s", dep.Name, dep.Version)
		deps = append(deps, line)
	}
	sort.Strings(deps)
	return strings.Join(deps, ", ")
}

func DecodeHelmRelease(data string) (*rspb.Release, error) {
	b, err := base64.StdEncoding.DecodeString(data)
	if err != nil {
		return nil, err
	}
	if len(b) < 3 {
		return nil, errors.New("invalid release sign")
	}
	if bytes.Equal(b[0:3], magicGzip) {
		r, err := gzip.NewReader(bytes.NewReader(b))
		if err != nil {
			return nil, err
		}
		defer r.Close()
		b2, err := ioutil.ReadAll(r)
		if err != nil {
			fmt.Printf("ERR %v\n", err)
			return nil, err
		}
		b = b2
	}
	var rls rspb.Release
	if err := json.Unmarshal(b, &rls); err != nil {
		return nil, err
	}
	return &rls, nil
}
