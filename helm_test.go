package main

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDecodeHelmRelease(t *testing.T) {
	_, err := DecodeHelmRelease("")
	assert.Error(t, err)

	_, err = DecodeHelmRelease("ABCD")
	assert.Error(t, err)

	b, err := os.ReadFile("test_data/valid_release.txt")
	if assert.NoError(t, err) {
		_, err = DecodeHelmRelease(string(b))
		assert.NoError(t, err)
	}
}

func TestChartLockToString(t *testing.T) {
	b, err := os.ReadFile("test_data/valid_release.txt")
	if !assert.NoError(t, err) {
		return
	}

	release, err := DecodeHelmRelease(string(b))
	if !assert.NoError(t, err) {
		return
	}

	out := ChartLockToString(release.Chart.Lock, []string{"app-template"})
	assert.Equal(t, "app-template@1.19.0", out)
}
