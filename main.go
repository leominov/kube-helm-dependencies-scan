package main

import (
	"context"
	"flag"
	"fmt"
	"os"
	"strings"
	"text/tabwriter"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var (
	dependencyFilter = flag.String("dependency", "", "Filter by dependency")
	namespace        = flag.String("n", "", "Filter by namespace")
)

func main() {
	flag.Parse()

	cli, err := NewClientSet()
	if err != nil {
		panic(err)
	}

	secretList, err := cli.CoreV1().Secrets(*namespace).List(context.Background(), metav1.ListOptions{
		FieldSelector: "type=helm.sh/release.v1",
	})
	if err != nil {
		panic(err)
	}

	w := new(tabwriter.Writer)
	w.Init(os.Stdout, 0, 8, 0, '\t', 0)
	_, _ = fmt.Fprintln(w, "Namespace\tRelease\tData\tDependencies")

	for _, secret := range secretList.Items {
		releaseRaw, ok := secret.Data["release"]
		if !ok {
			continue
		}
		release, err := DecodeHelmRelease(string(releaseRaw))
		if err != nil {
			fmt.Printf("ERR %v\n", err)
			continue
		}
		var filter []string
		for _, dep := range release.Chart.Metadata.Dependencies {
			if !dep.Enabled {
				continue
			}
			filter = append(filter, dep.Name)
		}
		if *dependencyFilter != "" {
			filter = []string{*dependencyFilter}
		}
		deps := ChartLockToString(release.Chart.Lock, filter)
		if deps == "" {
			continue
		}
		secretName := strings.TrimPrefix(secret.Name, "sh.helm.release.v1.")
		_, _ = fmt.Fprintf(w, "%s\t%s\t%s\t%s\n", secret.ObjectMeta.Namespace, secretName, secret.CreationTimestamp.String(), deps)
	}

	w.Flush()
}
