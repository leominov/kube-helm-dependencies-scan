# kube-helm-dependencies-scan

Список зависимостей опубликованных Helm-релизов.

## Применение

```shell
Usage of ./kube-helm-dependencies-scan:
  -dependency string
    	Filter by dependency
```

## Пример отчета

```text
Namespace			Release							Data				Dependencies
app-bff-self-service-form-app	bff-self-service-form-app-develop.v1			2021-10-11 17:03:24 +0500 +05	app-template@1.7.13
app-bff-self-service-form-app	bff-self-service-form-app-develop.v2			2021-10-11 17:07:20 +0500 +05	app-template@1.7.13
app-bff-self-service-form-app	bff-self-service-form-app-master.v1			2021-08-20 13:05:20 +0500 +05	app-template@1.7.7, postgresql@9.8.12
app-cleaner-app-bff		cleaner-app-bff-add-bundle-to-dockerfile.v1		2021-09-01 16:36:59 +0500 +05	app-template@1.7.13
app-cleaner-app-bff		cleaner-app-bff-master.v10				2021-09-01 17:34:57 +0500 +05	app-template@1.7.13
app-cleaner-app-bff		cleaner-app-bff-master.v11				2021-09-02 10:48:29 +0500 +05	app-template@1.7.13
app-cleaner-app-bff		cleaner-app-bff-master.v12				2021-09-02 11:04:29 +0500 +05	app-template@1.7.13
app-cleaner-app-bff		cleaner-app-bff-master.v13				2021-09-02 11:37:59 +0500 +05	app-template@1.7.13
app-cleaner-app-bff		cleaner-app-bff-master.v14				2021-09-03 10:29:31 +0500 +05	app-template@1.7.13
app-cleaner-app-bff		cleaner-app-bff-master.v15				2021-09-03 11:09:01 +0500 +05	app-template@1.7.13
app-cleaner-app-bff		cleaner-app-bff-master.v16				2021-09-07 13:08:52 +0500 +05	app-template@1.7.13
app-cleaner-app-bff		cleaner-app-bff-master.v17				2021-09-09 12:09:05 +0500 +05	app-template@1.7.13
app-cleaner-app-bff		cleaner-app-bff-master.v18				2021-09-10 14:16:35 +0500 +05	app-template@1.7.13
app-cleaner-app-bff		cleaner-app-bff-master.v19				2021-09-10 15:18:34 +0500 +05	app-template@1.7.13
app-contractor-app-bff		contractor-app-bff-master.v1				2021-09-10 14:14:34 +0500 +05	app-template@1.7.13
app-crm-calculator-svs		crm-calculator-svs-crm-1783.v1				2021-10-05 20:17:38 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-calculator-svs		crm-calculator-svs-master.v10				2021-07-27 16:49:32 +0500 +05	app-template@1.7.7, postgresql@9.8.12
app-crm-calculator-svs		crm-calculator-svs-master.v11				2021-08-17 14:20:38 +0500 +05	app-template@1.7.7, postgresql@9.8.12
app-crm-calculator-svs		crm-calculator-svs-master.v12				2021-08-17 14:29:38 +0500 +05	app-template@1.7.7, postgresql@9.8.12
app-crm-calculator-svs		crm-calculator-svs-master.v13				2021-09-08 17:13:28 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-calculator-svs		crm-calculator-svs-master.v14				2021-09-09 14:29:06 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-calculator-svs		crm-calculator-svs-master.v15				2021-09-09 15:04:06 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-calculator-svs		crm-calculator-svs-master.v16				2021-09-29 01:15:06 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-calculator-svs		crm-calculator-svs-master.v17				2021-10-06 13:49:06 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-calculator-svs		crm-calculator-svs-master.v8				2021-07-21 15:28:24 +0500 +05	app-template@1.7.7, postgresql@9.8.12
app-crm-calculator-svs		crm-calculator-svs-master.v9				2021-07-21 15:38:55 +0500 +05	app-template@1.7.7, postgresql@9.8.12
app-crm-feedback-svc		crm-feedback-svc-master.v6				2021-07-05 17:55:35 +0500 +05	app-template@1.7.7, postgresql@9.8.12
app-crm-feedback-svc		crm-feedback-svc-master.v7				2021-07-09 17:24:23 +0500 +05	app-template@1.7.7, postgresql@9.8.12
app-crm-feedback-svc		crm-feedback-svc-master.v8				2021-07-12 13:44:34 +0500 +05	app-template@1.7.7, postgresql@9.8.12
app-crm-feedback-svc		crm-feedback-svc-master.v9				2021-07-21 19:13:24 +0500 +05	app-template@1.7.7, postgresql@9.8.12
app-crm-mail-to-leads-svs	crm-mail-to-leads-svs-develop.v1			2021-10-01 17:48:17 +0500 +05	app-template@1.7.13, redis@11.3.4
app-crm-mail-to-leads-svs	crm-mail-to-leads-svs-develop.v2			2021-10-12 17:47:08 +0500 +05	app-template@1.7.13, redis@11.3.4
app-crm-mail-to-leads-svs	crm-mail-to-leads-svs-master.v35			2021-07-15 17:25:35 +0500 +05	app-template@1.7.7, redis@11.3.4
app-crm-mail-to-leads-svs	crm-mail-to-leads-svs-master.v36			2021-07-15 17:27:38 +0500 +05	app-template@1.7.7, redis@11.3.4
app-crm-mail-to-leads-svs	crm-mail-to-leads-svs-master.v37			2021-07-15 20:09:09 +0500 +05	app-template@1.7.7, redis@11.3.4
app-crm-mail-to-leads-svs	crm-mail-to-leads-svs-master.v38			2021-10-05 19:26:36 +0500 +05	app-template@1.7.13, redis@11.3.4
app-crm-mail-to-leads-svs	crm-mail-to-leads-svs-master.v39			2021-10-12 17:58:07 +0500 +05	app-template@1.7.13, redis@11.3.4
app-crm-regions-svs		crm-regions-svs-crm-1655.v1				2021-07-29 16:26:25 +0500 +05	app-template@1.7.7, postgresql@9.8.12
app-crm-regions-svs		crm-regions-svs-crm-1663.v1				2021-07-29 19:30:19 +0500 +05	app-template@1.7.7, postgresql@9.8.12
app-crm-regions-svs		crm-regions-svs-crm-1663.v2				2021-07-29 19:37:46 +0500 +05	app-template@1.7.7, postgresql@9.8.12
app-crm-regions-svs		crm-regions-svs-crm-1663.v3				2021-07-30 14:59:27 +0500 +05	app-template@1.7.7, postgresql@9.8.12
app-crm-regions-svs		crm-regions-svs-develop.v1				2021-10-06 23:12:06 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-regions-svs		crm-regions-svs-develop.v2				2021-10-11 16:30:24 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-regions-svs		crm-regions-svs-feature-crm-1435.v1			2021-07-01 15:42:32 +0500 +05	app-template@1.7.5, postgresql@9.8.12
app-crm-regions-svs		crm-regions-svs-feature-geojson-export.v1		2021-07-30 00:22:19 +0500 +05	app-template@1.7.7, postgresql@9.8.12
app-crm-regions-svs		crm-regions-svs-fin-1230.v1				2021-07-29 18:36:48 +0500 +05	app-template@1.7.7, postgresql@9.8.12
app-crm-regions-svs		crm-regions-svs-master.v1				2021-08-31 21:42:57 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-regions-svs		crm-regions-svs-new-ci.v1				2021-06-30 16:50:34 +0500 +05	app-template@1.7.5, postgresql@9.8.12
app-crm-sales-engine-svs	crm-sales-engine-svs-crm-1710.v10			2021-09-29 19:59:42 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-sales-engine-svs	crm-sales-engine-svs-crm-1710.v11			2021-09-30 14:42:11 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-sales-engine-svs	crm-sales-engine-svs-crm-1710.v2			2021-09-27 18:01:49 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-sales-engine-svs	crm-sales-engine-svs-crm-1710.v3			2021-09-27 18:28:49 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-sales-engine-svs	crm-sales-engine-svs-crm-1710.v4			2021-09-27 19:07:49 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-sales-engine-svs	crm-sales-engine-svs-crm-1710.v5			2021-09-27 19:45:49 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-sales-engine-svs	crm-sales-engine-svs-crm-1710.v6			2021-09-28 18:22:38 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-sales-engine-svs	crm-sales-engine-svs-crm-1710.v7			2021-09-28 19:35:08 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-sales-engine-svs	crm-sales-engine-svs-crm-1710.v8			2021-09-28 20:04:38 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-sales-engine-svs	crm-sales-engine-svs-crm-1710.v9			2021-09-29 19:24:42 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-sales-engine-svs	crm-sales-engine-svs-master.v1				2021-10-12 13:49:37 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-sales-engine-svs	crm-sales-engine-svs-master.v2				2021-10-12 14:53:37 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-sales-engine-svs	crm-sales-engine-svs-master.v3				2021-10-12 17:21:38 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-sales-engine-svs	crm-sales-engine-svs-master.v4				2021-10-12 19:47:14 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-task-manager		crm-task-manager-dev.v1					2021-09-26 18:02:32 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-task-manager		crm-task-manager-dev.v10				2021-10-06 21:14:59 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-task-manager		crm-task-manager-dev.v2					2021-09-28 20:54:37 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-task-manager		crm-task-manager-dev.v3					2021-09-30 12:53:11 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-task-manager		crm-task-manager-dev.v4					2021-10-01 22:31:47 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-task-manager		crm-task-manager-dev.v5					2021-10-05 12:54:30 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-task-manager		crm-task-manager-dev.v6					2021-10-05 13:17:30 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-task-manager		crm-task-manager-dev.v7					2021-10-05 14:19:00 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-task-manager		crm-task-manager-dev.v8					2021-10-05 16:39:16 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-task-manager		crm-task-manager-dev.v9					2021-10-06 20:38:04 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-task-manager		crm-task-manager-feature-add--mapper.v1			2021-09-01 18:59:00 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-task-manager		crm-task-manager-feature-add--mapper.v2			2021-09-02 12:38:01 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-task-manager		crm-task-manager-feature-crm-1792.v1			2021-10-11 18:55:12 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-task-manager		crm-task-manager-feature-crm-1792.v2			2021-10-11 19:30:41 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-task-manager		crm-task-manager-hotfix-add--state--transition.v1	2021-10-01 22:39:19 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-task-manager		crm-task-manager-master.v18				2021-10-05 13:27:59 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-task-manager		crm-task-manager-master.v19				2021-10-05 14:19:59 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-task-manager		crm-task-manager-master.v20				2021-10-05 16:39:29 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-task-manager		crm-task-manager-master.v21				2021-10-06 21:15:33 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-task-manager		crm-task-manager-master.v22				2021-10-07 19:08:20 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-task-manager		crm-task-manager-master.v23				2021-10-07 21:26:50 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-task-manager		crm-task-manager-master.v24				2021-10-08 17:26:28 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-task-manager		crm-task-manager-master.v25				2021-10-08 18:31:06 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-task-manager		crm-task-manager-master.v26				2021-10-11 13:38:52 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-task-manager		crm-task-manager-master.v27				2021-10-12 12:39:37 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-crm-task-svs		crm-task-svs-fix.v1					2021-09-02 15:38:34 +0500 +05	app-template@1.7.13, postgresql@9.8.12, redis@11.3.4
app-crm-task-svs		crm-task-svs-master.v23					2021-08-19 21:45:52 +0500 +05	app-template@1.7.7, postgresql@9.8.12, redis@11.3.4
app-crm-task-svs		crm-task-svs-master.v24					2021-08-20 15:28:51 +0500 +05	app-template@1.7.7, postgresql@9.8.12, redis@11.3.4
app-crm-task-svs		crm-task-svs-master.v25					2021-08-24 19:56:14 +0500 +05	app-template@1.7.12, postgresql@9.8.12, redis@11.3.4
app-crm-task-svs		crm-task-svs-master.v26					2021-08-26 14:12:31 +0500 +05	app-template@1.7.12, postgresql@9.8.12, redis@11.3.4
app-crm-task-svs		crm-task-svs-master.v27					2021-08-30 19:27:04 +0500 +05	app-template@1.7.13, postgresql@9.8.12, redis@11.3.4
app-crm-task-svs		crm-task-svs-master.v28					2021-08-30 20:00:28 +0500 +05	app-template@1.7.13, postgresql@9.8.12, redis@11.3.4
app-crm-task-svs		crm-task-svs-master.v29					2021-09-02 20:18:00 +0500 +05	app-template@1.7.13, postgresql@9.8.12, redis@11.3.4
app-crm-task-svs		crm-task-svs-master.v30					2021-09-15 14:30:27 +0500 +05	app-template@1.7.13, postgresql@9.8.12, redis@11.3.4
app-crm-task-svs		crm-task-svs-master.v31					2021-09-16 20:27:44 +0500 +05	app-template@1.7.13, postgresql@9.8.12, redis@11.3.4
app-crm-task-svs		crm-task-svs-master.v32					2021-09-29 16:34:40 +0500 +05	app-template@1.7.13, postgresql@9.8.12, redis@11.3.4
app-finance-acquiring-svs	finance-acquiring-svs-fin-1369.v10			2021-10-07 20:53:52 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-finance-acquiring-svs	finance-acquiring-svs-fin-1369.v11			2021-10-07 21:07:52 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-finance-acquiring-svs	finance-acquiring-svs-fin-1369.v2			2021-10-05 21:00:36 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-finance-acquiring-svs	finance-acquiring-svs-fin-1369.v3			2021-10-06 13:58:07 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-finance-acquiring-svs	finance-acquiring-svs-fin-1369.v4			2021-10-06 20:09:04 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-finance-acquiring-svs	finance-acquiring-svs-fin-1369.v5			2021-10-07 16:29:05 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-finance-acquiring-svs	finance-acquiring-svs-fin-1369.v6			2021-10-07 16:33:35 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-finance-acquiring-svs	finance-acquiring-svs-fin-1369.v7			2021-10-07 20:18:22 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-finance-acquiring-svs	finance-acquiring-svs-fin-1369.v8			2021-10-07 20:21:22 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-finance-acquiring-svs	finance-acquiring-svs-fin-1369.v9			2021-10-07 20:53:25 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-finance-acquiring-svs	finance-acquiring-svs-master.v36			2021-10-07 16:18:04 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-finance-acquiring-svs	finance-acquiring-svs-master.v37			2021-10-07 16:40:07 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-finance-acquiring-svs	finance-acquiring-svs-master.v38			2021-10-07 20:18:25 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-finance-acquiring-svs	finance-acquiring-svs-master.v39			2021-10-07 20:53:52 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-finance-acquiring-svs	finance-acquiring-svs-master.v40			2021-10-07 21:07:21 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-finance-invoice-svs		finance-invoice-svs-master.v30				2021-07-22 18:19:33 +0500 +05	app-template@1.7.7, postgresql@9.8.12, redis@11.3.4
app-finance-invoice-svs		finance-invoice-svs-master.v31				2021-07-22 18:21:08 +0500 +05	app-template@1.7.7, postgresql@9.8.12, redis@11.3.4
app-finance-invoice-svs		finance-invoice-svs-master.v32				2021-07-22 18:31:05 +0500 +05	app-template@1.7.7, postgresql@9.8.12, redis@11.3.4
app-finance-invoice-svs		finance-invoice-svs-switch-to-replicascount-1.v1	2021-08-23 16:23:25 +0500 +05	app-template@1.7.12, postgresql@9.8.12, redis@11.3.4
app-finance-invoice		finance-invoice-fin-1371.v1				2021-10-08 20:34:39 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-finance-invoice		finance-invoice-fin-1371.v2				2021-10-12 16:04:38 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-finance-invoice		finance-invoice-fin-1406.v1				2021-10-12 20:34:09 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-finance-invoice		finance-invoice-fin-1406.v2				2021-10-12 20:57:38 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-finance-invoice		finance-invoice-fin-1406.v3				2021-10-13 01:20:41 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-finance-invoice		finance-invoice-master.v73				2021-10-12 16:24:08 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-finance-invoice		finance-invoice-new-tests.v1				2021-04-13 13:28:42 +0500 +05	app-template@1.6.3, postgresql@9.8.12, rabbitmq@7.6.4
app-finance-receipts-svc	finance-receipts-svc-master.v4				2021-07-21 13:34:53 +0500 +05	app-template@1.7.7, postgresql@9.8.12, rabbitmq@7.6.4
app-finance-receipts-svc	finance-receipts-svc-master.v5				2021-07-28 19:51:54 +0500 +05	app-template@1.7.7, postgresql@9.8.12, rabbitmq@7.6.4
app-finance-receipts-svc	finance-receipts-svc-master.v6				2021-08-20 12:40:22 +0500 +05	app-template@1.7.7, postgresql@9.8.12, rabbitmq@7.6.4
app-finance-recurrent		finance-recurrent-fin-1273-new-pipeline.v1		2021-08-20 13:53:22 +0500 +05	app-template@1.7.7, postgresql@9.8.12
app-finance-recurrent		finance-recurrent-fin-1273-new-pipeline.v2		2021-08-20 16:57:52 +0500 +05	app-template@1.7.12, postgresql@9.8.12
app-finance-recurrent		finance-recurrent-fin-1273.v1				2021-08-20 17:35:22 +0500 +05	app-template@1.7.12, postgresql@9.8.12
app-finance-recurrent		finance-recurrent-fin-1273.v2				2021-08-21 15:03:52 +0500 +05	app-template@1.7.12, postgresql@9.8.12
app-finance-recurrent		finance-recurrent-fin-1273.v3				2021-08-23 12:28:59 +0500 +05	app-template@1.7.12, postgresql@9.8.12
app-finance-recurrent		finance-recurrent-fin-1273.v4				2021-08-23 12:35:22 +0500 +05	app-template@1.7.12, postgresql@9.8.12
app-finance-recurrent		finance-recurrent-fin-1273.v5				2021-08-23 13:00:22 +0500 +05	app-template@1.7.12, postgresql@9.8.12
app-finance-recurrent		finance-recurrent-fin-1273.v6				2021-08-23 13:54:23 +0500 +05	app-template@1.7.12, postgresql@9.8.12
app-finance-recurrent		finance-recurrent-fin-1273.v7				2021-08-23 14:13:23 +0500 +05	app-template@1.7.12, postgresql@9.8.12
app-finance-recurrent		finance-recurrent-fin-1273.v8				2021-08-23 14:26:53 +0500 +05	app-template@1.7.12, postgresql@9.8.12
app-finance-recurrent		finance-recurrent-fin-1277-tests.v1			2021-08-27 17:36:51 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-finance-recurrent		finance-recurrent-fin-1277-tests.v2			2021-08-27 18:09:20 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-finance-recurrent		finance-recurrent-fin-1277-tests.v3			2021-08-27 20:16:50 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-finance-recurrent		finance-recurrent-fin-1277.v1				2021-08-27 15:01:20 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-finance-recurrent		finance-recurrent-fin-1401.v1				2021-10-06 17:38:14 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-finance-recurrent		finance-recurrent-master.v10				2021-08-27 20:28:49 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-finance-recurrent		finance-recurrent-master.v11				2021-08-31 11:31:56 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-finance-recurrent		finance-recurrent-master.v2				2021-08-20 17:33:51 +0500 +05	app-template@1.7.12, postgresql@9.8.12
app-finance-recurrent		finance-recurrent-master.v3				2021-08-27 14:10:14 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-finance-recurrent		finance-recurrent-master.v4				2021-08-27 15:09:18 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-finance-recurrent		finance-recurrent-master.v5				2021-08-27 15:11:18 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-finance-recurrent		finance-recurrent-master.v6				2021-08-27 17:46:49 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-finance-recurrent		finance-recurrent-master.v7				2021-08-27 18:09:20 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-finance-recurrent		finance-recurrent-master.v8				2021-08-27 18:51:19 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-finance-recurrent		finance-recurrent-master.v9				2021-08-27 18:57:49 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-finance-recurrent		finance-recurrent-new-ci.v1				2021-08-20 16:01:27 +0500 +05	app-template@1.7.10, postgresql@9.8.12
app-finance-recurrent		finance-recurrent-new-ci.v2				2021-08-20 16:32:27 +0500 +05	app-template@1.7.12, postgresql@9.8.12
app-hrm-core-svc		hrm-core-svc-master.v1					2021-10-08 17:21:28 +0500 +05	app-template@1.7.13, postgresql@9.8.12, redis@11.3.4
app-hrm-core-svc		hrm-core-svc-master.v2					2021-10-11 17:25:22 +0500 +05	app-template@1.7.13, postgresql@9.8.12, redis@11.3.4
app-platform-admin-web		platform-admin-web-feature-adm-726.v1			2021-10-11 16:01:26 +0500 +05	app-template@1.7.13
app-platform-admin-web		platform-admin-web-feature-adm-730.v1			2021-10-08 20:55:40 +0500 +05	app-template@1.7.13
app-platform-admin-web		platform-admin-web-feature-adm-731.v1			2021-10-11 16:08:55 +0500 +05	app-template@1.7.13
app-platform-admin-web		platform-admin-web-foo.v1				2021-09-13 20:18:20 +0500 +05	app-template@1.7.13
app-platform-admin-web		platform-admin-web-master.v28				2021-09-21 11:47:48 +0500 +05	app-template@1.7.13
app-platform-admin-web		platform-admin-web-master.v29				2021-09-21 14:06:22 +0500 +05	app-template@1.7.13
app-platform-admin-web		platform-admin-web-master.v30				2021-09-21 14:41:20 +0500 +05	app-template@1.7.13
app-platform-admin-web		platform-admin-web-master.v31				2021-09-21 15:23:48 +0500 +05	app-template@1.7.13
app-platform-admin-web		platform-admin-web-master.v32				2021-09-21 15:51:48 +0500 +05	app-template@1.7.13
app-platform-admin-web		platform-admin-web-plt-core-v2.v1			2021-10-05 21:29:08 +0500 +05	app-template@1.7.13
app-platform-admin-web		platform-admin-web-plt-core-v2.v2			2021-10-08 15:51:29 +0500 +05	app-template@1.7.13
app-platform-admin-web		platform-admin-web-plt-core-v2.v3			2021-10-08 16:23:30 +0500 +05	app-template@1.7.13
app-platform-admin-web		platform-admin-web-plt-core-v2.v4			2021-10-08 19:03:08 +0500 +05	app-template@1.7.13
app-platform-admin-web		platform-admin-web-plt-core-v2.v5			2021-10-08 21:03:38 +0500 +05	app-template@1.7.13
app-platform-admin-web		platform-admin-web-plt-core-v2.v6			2021-10-09 01:27:08 +0500 +05	app-template@1.7.13
app-platform-admin-web		platform-admin-web-plt-core-v2.v7			2021-10-11 01:37:24 +0500 +05	app-template@1.7.13
app-platform-crm-executors-web	platform-crm-executors-web-feature-new--pipeline.v1	2021-10-04 15:00:35 +0500 +05	app-template@1.7.13
app-platform-crm-executors-web	platform-crm-executors-web-feature-new--pipeline.v2	2021-10-04 15:12:33 +0500 +05	app-template@1.7.13
app-platform-crm-executors-web	platform-crm-executors-web-feature-new--pipeline.v3	2021-10-04 16:32:03 +0500 +05	app-template@1.7.13
app-platform-system-core	platform-system-core-release-plt-core-v0-1-x.v1		2021-07-28 13:31:36 +0500 +05	app-template@1.7.7, postgresql@9.8.12
app-platform-system-core	platform-system-core-release-v0-1-x.v1			2021-07-28 13:42:35 +0500 +05	app-template@1.7.7, postgresql@9.8.12
app-platform-system-core	platform-system-core-release-v0-1-x.v2			2021-07-30 09:55:48 +0500 +05	app-template@1.7.7, postgresql@9.8.12
app-platform-system-core	platform-system-core-release-v0-1-x.v3			2021-08-10 10:48:01 +0500 +05	app-template@1.7.7, postgresql@9.8.12
app-service-bus			service-bus-master.v10					2021-08-17 18:49:08 +0500 +05	app-template@1.7.7, postgresql@9.8.12, rabbitmq@7.6.4
app-service-bus			service-bus-master.v4					2021-07-07 16:58:50 +0500 +05	app-template@1.7.7, postgresql@9.8.12, rabbitmq@7.6.4
app-service-bus			service-bus-master.v5					2021-07-08 22:10:22 +0500 +05	app-template@1.7.7, postgresql@9.8.12, rabbitmq@7.6.4
app-service-bus			service-bus-master.v6					2021-07-09 17:10:52 +0500 +05	app-template@1.7.7, postgresql@9.8.12, rabbitmq@7.6.4
app-service-bus			service-bus-master.v7					2021-07-12 13:33:04 +0500 +05	app-template@1.7.7, postgresql@9.8.12, rabbitmq@7.6.4
app-service-bus			service-bus-master.v8					2021-07-19 20:01:47 +0500 +05	app-template@1.7.7, postgresql@9.8.12, rabbitmq@7.6.4
app-service-bus			service-bus-master.v9					2021-07-23 13:47:22 +0500 +05	app-template@1.7.7, postgresql@9.8.12, rabbitmq@7.6.4
app-sso-identity-2-svs		sso-identity-2-svs-adds-meda-data-description.v1	2021-10-04 15:25:53 +0500 +05	app-template@1.7.13, postgresql@9.8.12, redis@11.3.4
app-sso-identity-2-svs		sso-identity-2-svs-adds-meda-data-description.v2	2021-10-04 15:26:05 +0500 +05	app-template@1.7.13, postgresql@9.8.12, redis@11.3.4
app-sso-identity-2-svs		sso-identity-2-svs-adds-meta.v1				2021-09-02 13:17:36 +0500 +05	app-template@1.7.13, postgresql@9.8.12, redis@11.3.4
app-sso-identity-2-svs		sso-identity-2-svs-delete-by-uid.v1			2021-09-22 12:11:45 +0500 +05	app-template@1.7.13, postgresql@9.8.12, redis@11.3.4
app-sso-identity-2-svs		sso-identity-2-svs-disable-rfrsh-tkn-invl.v1		2021-07-12 13:47:09 +0500 +05	app-template@1.7.7, postgresql@9.8.12, redis@11.3.4
app-sso-identity-2-svs		sso-identity-2-svs-master.v37				2021-08-25 14:06:05 +0500 +05	app-template@1.7.12, postgresql@9.8.12, redis@11.3.4
app-sso-identity-2-svs		sso-identity-2-svs-master.v38				2021-08-30 13:36:41 +0500 +05	app-template@1.7.13, postgresql@9.8.12, redis@11.3.4
app-sso-identity-2-svs		sso-identity-2-svs-master.v39				2021-08-30 13:41:41 +0500 +05	app-template@1.7.13, postgresql@9.8.12, redis@11.3.4
app-sso-identity-2-svs		sso-identity-2-svs-master.v40				2021-08-30 17:13:33 +0500 +05	app-template@1.7.13, postgresql@9.8.12, redis@11.3.4
app-sso-identity-2-svs		sso-identity-2-svs-master.v41				2021-08-30 19:56:00 +0500 +05	app-template@1.7.13, postgresql@9.8.12, redis@11.3.4
app-sso-identity-2-svs		sso-identity-2-svs-master.v42				2021-09-02 13:51:31 +0500 +05	app-template@1.7.13, postgresql@9.8.12, redis@11.3.4
app-sso-identity-2-svs		sso-identity-2-svs-master.v43				2021-09-06 13:19:10 +0500 +05	app-template@1.7.13, postgresql@9.8.12, redis@11.3.4
app-sso-identity-2-svs		sso-identity-2-svs-master.v44				2021-09-10 12:52:36 +0500 +05	app-template@1.7.13, postgresql@9.8.12, redis@11.3.4
app-sso-identity-2-svs		sso-identity-2-svs-master.v45				2021-10-04 15:36:33 +0500 +05	app-template@1.7.13, postgresql@9.8.12, redis@11.3.4
app-sso-identity-2-svs		sso-identity-2-svs-master.v46				2021-10-11 17:33:24 +0500 +05	app-template@1.7.13, postgresql@9.8.12, redis@11.3.4
app-sso-identity-2-svs		sso-identity-2-svs-profile-385.v1			2021-08-27 21:00:55 +0500 +05	app-template@1.7.13, postgresql@9.8.12, redis@11.3.4
app-sso-identity-2-svs		sso-identity-2-svs-profile-486-jwks.v1			2021-09-22 09:01:45 +0500 +05	app-template@1.7.13, postgresql@9.8.12, redis@11.3.4
app-sso-identity-2-svs		sso-identity-2-svs-profile-515-logchanges.v1		2021-09-08 09:30:04 +0500 +05	app-template@1.7.13, postgresql@9.8.12, redis@11.3.4
app-sso-identity-2-svs		sso-identity-2-svs-profile-515lgop.v1			2021-09-08 12:04:34 +0500 +05	app-template@1.7.13, postgresql@9.8.12, redis@11.3.4
app-sso-identity-2-svs		sso-identity-2-svs-profile-515lgpr.v1			2021-09-09 11:52:41 +0500 +05	app-template@1.7.13, postgresql@9.8.12, redis@11.3.4
app-sso-identity-2-svs		sso-identity-2-svs-profile-543.v1			2021-08-09 18:05:30 +0500 +05	app-template@1.7.7, postgresql@9.8.12, redis@11.3.4
app-sso-identity-2-svs		sso-identity-2-svs-profile-543.v2			2021-08-09 18:14:58 +0500 +05	app-template@1.7.7, postgresql@9.8.12, redis@11.3.4
app-sso-identity-2-svs		sso-identity-2-svs-profile-544-ssoutils.v1		2021-07-29 12:24:30 +0500 +05	app-template@1.7.7, postgresql@9.8.12, redis@11.3.4
app-sso-identity-2-svs		sso-identity-2-svs-profile-544-ssoutils.v2		2021-07-29 12:40:27 +0500 +05	app-template@1.7.7, postgresql@9.8.12, redis@11.3.4
app-sso-identity-2-svs		sso-identity-2-svs-profile-544-ssoutils.v3		2021-07-29 12:52:30 +0500 +05	app-template@1.7.7, postgresql@9.8.12, redis@11.3.4
app-sso-identity-2-svs		sso-identity-2-svs-profile-544-ssoutils.v4		2021-07-29 21:03:18 +0500 +05	app-template@1.7.7, postgresql@9.8.12, redis@11.3.4
app-sso-identity-2-svs		sso-identity-2-svs-profile-544-ssoutils.v5		2021-08-17 16:36:11 +0500 +05	app-template@1.7.7, postgresql@9.8.12, redis@11.3.4
app-sso-identity-2-svs		sso-identity-2-svs-profile-548-photomgrt.v1		2021-08-11 11:46:59 +0500 +05	app-template@1.7.7, postgresql@9.8.12, redis@11.3.4
app-sso-identity-2-svs		sso-identity-2-svs-profile-548-photomgrt.v2		2021-08-11 13:32:26 +0500 +05	app-template@1.7.7, postgresql@9.8.12, redis@11.3.4
app-sso-identity-2-svs		sso-identity-2-svs-profile-549.v1			2021-08-06 15:09:45 +0500 +05	app-template@1.7.7, postgresql@9.8.12, redis@11.3.4
app-sso-identity-2-svs		sso-identity-2-svs-profile-550-fiocorrect.v1		2021-08-19 13:02:03 +0500 +05	app-template@1.7.7, postgresql@9.8.12, redis@11.3.4
app-sso-identity-2-svs		sso-identity-2-svs-profile-552-updproffix.v1		2021-08-16 14:49:11 +0500 +05	app-template@1.7.7, postgresql@9.8.12, redis@11.3.4
app-sso-identity-2-svs		sso-identity-2-svs-profile-567.v1			2021-09-30 10:13:45 +0500 +05	app-template@1.7.13, postgresql@9.8.12, redis@11.3.4
app-sso-identity-2-svs		sso-identity-2-svs-profile-567.v2			2021-10-01 20:54:19 +0500 +05	app-template@1.7.13, postgresql@9.8.12, redis@11.3.4
app-sso-identity-2-svs		sso-identity-2-svs-profile-567.v3			2021-10-13 00:02:52 +0500 +05	app-template@1.7.13, postgresql@9.8.12, redis@11.3.4
app-sso-identity-2-svs		sso-identity-2-svs-profile-567.v4			2021-10-13 08:31:48 +0500 +05	app-template@1.7.13, postgresql@9.8.12, redis@11.3.4
app-sso-identity-svs		sso-identity-svs-adds-service-meta.v1			2021-09-29 20:10:27 +0500 +05	app-template@1.7.13, postgresql@9.8.12, redis@11.3.4
app-sso-identity-svs		sso-identity-svs-master.v10				2021-07-11 14:10:36 +0500 +05	app-template@1.7.7, postgresql@9.8.12, redis@11.3.4
app-sso-identity-svs		sso-identity-svs-master.v11				2021-07-11 15:36:06 +0500 +05	app-template@1.7.7, postgresql@9.8.12, redis@11.3.4
app-sso-identity-svs		sso-identity-svs-master.v12				2021-07-11 15:43:06 +0500 +05	app-template@1.7.7, postgresql@9.8.12, redis@11.3.4
app-sso-identity-svs		sso-identity-svs-master.v13				2021-07-30 15:58:19 +0500 +05	app-template@1.7.7, postgresql@9.8.12, redis@11.3.4
app-sso-identity-svs		sso-identity-svs-master.v14				2021-08-03 15:24:20 +0500 +05	app-template@1.7.7, postgresql@9.8.12, redis@11.3.4
app-sso-identity-svs		sso-identity-svs-master.v15				2021-08-17 15:32:09 +0500 +05	app-template@1.7.7, postgresql@9.8.12, redis@11.3.4
app-sso-identity-svs		sso-identity-svs-master.v9				2021-07-11 14:08:06 +0500 +05	app-template@1.7.7, postgresql@9.8.12, redis@11.3.4
app-sso-profile-svs		sso-profile-svs-adds-service-meta.v1			2021-09-29 20:10:43 +0500 +05	app-template@1.7.13, postgresql@9.8.12
app-sso-profile-svs		sso-profile-svs-master.v23				2021-07-11 13:40:36 +0500 +05	app-template@1.7.7, postgresql@9.8.12
app-sso-profile-svs		sso-profile-svs-master.v24				2021-07-11 15:23:40 +0500 +05	app-template@1.7.7, postgresql@9.8.12
app-web-widget-profile-info	web-widget-profile-info-master.v1			2021-10-12 14:22:37 +0500 +05	app-template@1.7.13
app-web-widget-profile-info	web-widget-profile-info-master.v2			2021-10-12 20:26:07 +0500 +05	app-template@1.7.13
app-web-widget-profile-info	web-widget-profile-info-master.v3			2021-10-12 20:48:36 +0500 +05	app-template@1.7.13
app-web-widget-reglament	web-widget-reglament-master.v1				2021-10-08 12:44:54 +0500 +05	app-template@1.7.13
app-web-widget-reglament	web-widget-reglament-master.v2				2021-10-08 13:13:57 +0500 +05	app-template@1.7.13
kube-system			external-dns.v1						2020-12-14 18:13:36 +0500 +05	common@1.1.1
rabbitmq			stage.v1						2021-06-15 21:37:14 +0500 +05	common@1.0.0
```
